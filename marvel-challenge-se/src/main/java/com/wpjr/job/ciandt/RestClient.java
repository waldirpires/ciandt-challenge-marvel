package com.wpjr.job.ciandt;

import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

import us.monoid.web.Resty;

public class RestClient
{

    public static void main(String[] args) throws IOException
    {
        String publicKey = "19554a34e7f49a1956e56ce9e2cb99bc";
        String privateKey = "48149c914b0088cfc5b6b4cd91f793d596444b8a";
        long timeStamp = System.currentTimeMillis();
        int limit = 5;

        String stringToHash = timeStamp + privateKey + publicKey;
        String hash = DigestUtils.md5Hex(stringToHash);

        String url = String.format("http://gateway.marvel.com/v1/public/characters?ts=%d&apikey=%s&hash=%s&limit=%d", timeStamp, publicKey, hash, limit);
        System.out.println(url);
        String output = new Resty().text(url).toString();
        System.out.println(output);
    }
}
