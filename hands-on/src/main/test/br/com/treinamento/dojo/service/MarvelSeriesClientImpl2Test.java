package br.com.treinamento.dojo.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import br.com.treinamento.dojo.entity.MarvelEntityList;
import br.com.treinamento.dojo.entity.MarvelSeriesWithCharactersInfo;
import br.com.treinamento.dojo.utils.JsonUtils;

public class MarvelSeriesClientImpl2Test {

	private MarvelSeriesClientImpl2 client;

    @Before
    public void setup()
    {
        client = MarvelSeriesClientImpl2.getInstance();
    }
    
	@Test
    public void testgetSpecificCharacterSuccess(){
    	String id = "1009165";
    	JsonObject character = client.getSpecificCharacter(id);
    	Assert.assertNotNull(character);
    	Assert.assertEquals(1, character.getAsJsonObject("data").getAsJsonArray("results").size());
    	
    }
	
	@Test
    public void testgetSpecificCharacterNull(){
    	String id = null;
    	JsonObject character = client.getSpecificCharacter(id);
    	Assert.assertNull(character);    	
    }
	
	@Test
    public void testgetSpecificCharacterNotExist(){
    	String id = "blah";
    	JsonObject character = client.getSpecificCharacter(id);
    	Assert.assertNull(character);    	
    }
	
	@Test
    public void testGetSpecificSeries2(){
    	String id = "1945";
    	//http://gateway.marvel.com/v1/public/series/1945?ts=1478785656836&apikey=19554a34e7f49a1956e56ce9e2cb99bc&hash=da060e1e6f6e33c4e4e63c397aa9f3fa&limit=5
    	// characters.items.resourceURI
    	
    	JsonObject specificSeries = client.getSpecificSeries(id);
    	Assert.assertNotNull(specificSeries);
    	JsonObject element = JsonUtils.getUniqueObject(specificSeries);    			
    	JsonArray characters = element.getAsJsonObject("characters").getAsJsonArray("items");
    	for (int i = 0; i < characters.size(); i++){
    		JsonObject chr = characters.get(i).getAsJsonObject();    		
    		if (chr.get("resourceURI") != null){
    			String characterId = JsonUtils.getIdFromResourceUri(chr, "resourceURI");
    			JsonObject character = client.getSpecificCharacter(characterId);
    			character = JsonUtils.getUniqueObject(character);
    			System.out.println(character);
    		}
    	}
    }
    
	@Test
    public void testgetCompleteSeriesWithCharactersSuccess(){
    	String id = "1945";
    	int expected = 20;
    	MarvelSeriesWithCharactersInfo completeSeries = client.getCompleteSeriesWithCharacters(id);
    	Assert.assertNotNull(completeSeries);
    	Assert.assertTrue(!completeSeries.getCharacters().isEmpty());
    	Assert.assertEquals(expected, completeSeries.getCharacters().size());
    }
    
    @Test
    public void testgetCompleteSeriesWithCharactersNullId(){
    	MarvelSeriesWithCharactersInfo completeSeries = client.getCompleteSeriesWithCharacters(null);
    	Assert.assertNull(completeSeries);
    }
    
    @Test
    public void testgetCompleteSeriesWithCharactersInexistentId(){
    	
    	MarvelSeriesWithCharactersInfo completeSeries = 
    			client.getCompleteSeriesWithCharacters("blah");
    	Assert.assertNull(completeSeries);
    }
    
    @Test
    public void testgetAllCharacters(){
    	MarvelEntityList allCharacters = client.getAllCharacters(1, 1);
    	Assert.assertNotNull(allCharacters);
    	Assert.assertEquals(1, allCharacters.getSize());
    }

    @Test
    public void testgetAllSeries(){
    	MarvelEntityList list = client.getAllSeries(1, 1);
    	Assert.assertNotNull(list);
    	Assert.assertEquals(1, list.getSize());    	
    }
    
    @Test
    public void testgetAllComics(){
    	MarvelEntityList list = client.getAllComics(1, 1);
    	Assert.assertNotNull(list);
    	Assert.assertEquals(1, list.getSize());    
    }
    
    @Test
    public void testAddCharacter(){
    	String json = "{\"id\": \"1\"}";
    	boolean result = client.addCharacter(JsonUtils.getObjectFromString(json));
    	Assert.assertTrue(result);
    	JsonObject specificCharacter = client.getSpecificCharacter("1");
    	Assert.assertNotNull(specificCharacter);
    	
    	
    }

    @Test
    public void testUpdateCharacter(){
    	String json = "{\"id\":\"1\",\"name\":1}";
    	boolean result = client.addCharacter(JsonUtils.getObjectFromString(json));
    	Assert.assertTrue(result);
    	JsonObject obj = client.getSpecificCharacter("1");
    	Assert.assertNotNull(obj);
    	Assert.assertEquals(json, obj.toString());
    	
    	// update
    	json = "{\"id\":\"1\",\"name\":2}";
    	result = client.updateCharacter(JsonUtils.getObjectFromString(json));
    	Assert.assertTrue(result);
    	// get and compare
    	obj = client.getSpecificCharacter("1");
    	Assert.assertNotNull(obj);
    	Assert.assertEquals(json, obj.toString());
    	
    	
    }

    @Test
    public void testdeleteSpecific(){

    	// add
    	String json = "{\"id\":\"1\",\"name\":1}";
    	boolean result = client.addCharacter(JsonUtils.getObjectFromString(json));
    	Assert.assertTrue(result);
    	JsonObject obj = client.getSpecificCharacter("1");
    	Assert.assertNotNull(obj);
    	Assert.assertEquals(json, obj.toString());
    	
    	// delete
    	result = client.deleteCharacter(obj);
    	Assert.assertTrue(result);
    	
    	// get
    	obj = client.getSpecificCharacter("1");
    	Assert.assertNull(obj);
    	
    }
    
    @After
    public void tearDown(){
    	client.clearData();
    }
}
