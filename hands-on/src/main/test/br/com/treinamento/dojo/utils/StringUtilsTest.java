package br.com.treinamento.dojo.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

	@Test
	public void testIsEmpty(){
		String test = null;
		Assert.assertTrue(StringUtils.isEmpty(test));
		
		test = "";
		Assert.assertTrue(StringUtils.isEmpty(test));
		
		test = " ";
		Assert.assertTrue(StringUtils.isEmpty(test));
		
		test = ".";
		Assert.assertFalse(StringUtils.isEmpty(test));				
	}
}
