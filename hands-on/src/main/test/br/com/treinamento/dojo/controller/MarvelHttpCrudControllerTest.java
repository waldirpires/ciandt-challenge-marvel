package br.com.treinamento.dojo.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.google.gson.JsonObject;

import br.com.treinamento.dojo.service.MarvelSeriesClientImpl2;
import br.com.treinamento.dojo.utils.JsonUtils;

public class MarvelHttpCrudControllerTest {

	private MarvelHttpCrudController controller;
	
	@Before
	public void setup(){
		controller = new MarvelHttpCrudController();
	}
	
	@Test
	public void testcreateCharacter(){
		String json = "{\"id\":\"1\",\"name\":1}";
		ResponseEntity<Void> result = controller.createCharacter(json);
		Assert.assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}
	
	@Test
	public void testlistCharacters(){
		// list empty
		ResponseEntity<String> result = controller.listCharacters();
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		JsonObject obj = JsonUtils.getObjectFromString(result.getBody());
		Assert.assertEquals(0, obj.get("size").getAsInt());
		
		// list with one
		String json = "{\"id\":\"1\",\"name\":1}";
		ResponseEntity<Void> result2 = controller.createCharacter(json);
		Assert.assertEquals(HttpStatus.CREATED, result2.getStatusCode());
		
		result = controller.listCharacters();
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		obj = JsonUtils.getObjectFromString(result.getBody());
		Assert.assertEquals(1, obj.getAsJsonArray("list").size());
	}
	
	@Test
	public void testgetCharacter(){
		// nonexistent
		ResponseEntity<String> result = controller.getCharacter(1);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		
		// existent
		String json = "{\"id\":\"1\",\"name\":1}";
		ResponseEntity<Void> result2 = controller.createCharacter(json);
		Assert.assertEquals(HttpStatus.CREATED, result2.getStatusCode());
		ResponseEntity<String> result3 = controller.getCharacter(1);
		Assert.assertEquals(HttpStatus.OK, result3.getStatusCode());
	}
	
	@Test
	public void testupdateCharacter(){
		
	}
	
	
	@Test
	public void testdeleteCharacter(){
		
	}
	
	@Test
	public void testclearData(){
		
	}
	
	@After
	public void tearDown(){
		MarvelSeriesClientImpl2.getInstance().clearData();
	}
	
}
