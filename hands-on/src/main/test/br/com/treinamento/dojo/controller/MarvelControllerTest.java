package br.com.treinamento.dojo.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

public class MarvelControllerTest {

	private MarvelController controller;
	
	@Test
	public void testgetCompleteSeriesWithCharacters(){
		controller = new MarvelController();
		ResponseEntity<String> allCharacters = controller.getCompleteSeriesWithCharacters("1945");
		Assert.assertNotNull(allCharacters);
	}
	
	@Test
	public void testgetAllCharacters(){
		controller = new MarvelController();
		ResponseEntity<String> allCharacters = controller.getAllCharacters("1");
		Assert.assertNotNull(allCharacters);
		
	}

	@Test
	public void testgetAllSeries(){
		controller = new MarvelController();
		ResponseEntity<String> allCharacters = controller.getAllSeries("1");
		Assert.assertNotNull(allCharacters);
		
	}
}
