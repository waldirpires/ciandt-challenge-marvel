package br.com.treinamento.dojo.service;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DataServiceImplTest {

	private DataServiceImpl dataService;
	
	@Before
	public void setup(){
		dataService = new DataServiceImpl();
	}
	
	@Test
	public void testlist(){
		dataService.addToData("test1", "blah");
		dataService.addToData("test12", "blahblah");
		dataService.addToData("test13", "blahbleh");
		dataService.addToData("test2", "bluh");
		Set<Object> list = dataService.list("test1");
		Assert.assertEquals(3, list.size());
	}
}
