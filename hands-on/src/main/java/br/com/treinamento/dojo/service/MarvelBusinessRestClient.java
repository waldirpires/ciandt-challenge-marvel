//**********************************************************************
// Copyright (c) 2016 Telefonaktiebolaget LM Ericsson, Sweden.
// All rights reserved.
// The Copyright to the computer program(s) herein is the property of
// Telefonaktiebolaget LM Ericsson, Sweden.
// The program(s) may be used and/or copied with the written permission
// from Telefonaktiebolaget LM Ericsson or in accordance with the terms
// and conditions stipulated in the agreement/contract under which the
// program(s) have been supplied.
// **********************************************************************
package br.com.treinamento.dojo.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import br.com.treinamento.dojo.entity.characters.MarvelCharacterResponse;
import br.com.treinamento.dojo.entity.characters.ResultElement;
import br.com.treinamento.dojo.entity.series.MarvelSeriesResponse;

public class MarvelBusinessRestClient extends AbstractMarvelClientApi implements MarvelClientApi {
	private final static Logger LOG = Logger.getLogger(MarvelBusinessRestClient.class);

	private static MarvelBusinessRestClient instance;
	private Map<String, Object> data;

	private MarvelBusinessRestClient() {
		data = new HashMap<String, Object>();
	}

	public static MarvelBusinessRestClient getInstance() {
		if (instance == null) {
			instance = new MarvelBusinessRestClient();
		}
		return instance;
	}

	

	

	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getCharacter(java.lang.String)
	 */
	@Override
	public MarvelCharacterResponse getCharacter(String id) {
		//MarvelBusinessRestClient client = new MarvelBusinessRestClient();
		String operation = "characters/".concat(id);
		return getMarvelCharactersFromPayload(getPayloadFromResponse(this.executeGet(operation, 1, 0)));
	}
	
	
	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getAllCharacters(int, int)
	 */
	@Override
	public Set<ResultElement> getAllCharacters(int maxSize, int limit){
		// get first payload		
		MarvelCharacterResponse mcr = getMarvelCharactersFromPayload(getPayloadFromResponse(getCharacters(1, 0)));
		// get total amount
		int total = mcr.getData().getTotal().intValue();
		LOG.debug("Found: " + total + " entries");
		if (maxSize != -1 && total > maxSize){
			total = maxSize;
		}
		int steps = total / limit;
		if (total / limit !=0){
			steps++;
		}
		LOG.debug("Found: " + total + " entries | " + steps + " steps");
		//ResultElement [] results = new ResultElement[total];
		Set<ResultElement> results = new HashSet<>();
		for (int i = 0; i < steps; i++){
			LOG.info("Getting list of characters: " + i + " / " + steps);
			mcr = getMarvelCharactersFromPayload(getPayloadFromResponse(getCharacters(limit, i)));
			for (ResultElement re: mcr.getData().getResults()){
				results.add(re);
			}
		}
		LOG.info(results.size());
		// iterate through each offset
		return results;
		
	}
	
	
	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getMarvelCharactersFromPayload(java.lang.String)
	 */
	@Override
	public MarvelCharacterResponse getMarvelCharactersFromPayload(String payload)
	{
		Gson gson = new Gson();
		MarvelCharacterResponse mcr = gson.fromJson(payload, MarvelCharacterResponse.class);
		return mcr;
	}	
	

	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getSpecificSeries(java.lang.String)
	 */
	@Override
	public MarvelSeriesResponse getSpecificSeries(String id) {
		///MarvelBusinessRestClient client = new MarvelBusinessRestClient();
		String operation = "series/".concat(id);
		return getSeriesResponseFromPayload(getPayloadFromResponse(this.executeGet(operation, 1, 0)));
	}
	
	
	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getCharacterResponseFromPayload(java.lang.String)
	 */
	@Override
	public MarvelCharacterResponse getCharacterResponseFromPayload(String payload) {
		if (payload == null || payload.trim().isEmpty()) {
			return null;
		}
		Gson gson = new Gson();
		return gson.fromJson(payload, MarvelCharacterResponse.class);
	}

	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getSeriesResponseFromPayload(java.lang.String)
	 */
	@Override
	public MarvelSeriesResponse getSeriesResponseFromPayload(String payload) {
		if (payload == null || payload.trim().isEmpty()) {
			return null;
		}
		Gson gson = new Gson();
		return gson.fromJson(payload, MarvelSeriesResponse.class);
	}

}
