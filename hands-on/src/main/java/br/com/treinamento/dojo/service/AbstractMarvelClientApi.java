package br.com.treinamento.dojo.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import br.com.treinamento.dojo.entity.MarvelEntityList;
import br.com.treinamento.dojo.utils.JsonUtils;

public abstract class AbstractMarvelClientApi {
	private final static Logger LOG = Logger.getLogger(AbstractMarvelClientApi.class);
	protected static final String PUBLIC_KEY = "19554a34e7f49a1956e56ce9e2cb99bc";
	protected static final String PRIVATE_KEY_HASH = "da060e1e6f6e33c4e4e63c397aa9f3fa";
	protected static final String BASE_URL = "http://gateway.marvel.com/v1/public/";
	protected static final String PARAMS = "?ts=%s&apikey=%s&hash=%s&limit=%d&offset=%d";
	protected static final String TIMESTAMP = "1478785656836";
	
	public String getPayloadFromResponse(Response response) {
		response.bufferEntity();
		String readEntity = response.readEntity(String.class);
		LOG.debug("Response payload: " + readEntity.length());
		return readEntity;
	}

	public Response executeGet(String operation, int limit, int offset) {
		LOG.debug("Requesting operation: " + operation);
		Client c = ClientBuilder.newClient();

		String url = BASE_URL.concat(operation)
				.concat(String.format(PARAMS, TIMESTAMP, PUBLIC_KEY, PRIVATE_KEY_HASH, limit, offset));
		
		LOG.info("GET request: " + url);
		
		WebTarget webTarget = c.target(url);
		
		System.out.println(webTarget.getUri().toString());
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		long time = System.currentTimeMillis();
		Response response = invocationBuilder.get();
		LOG.info("RESPONSE: " + response.getStatus() + "|" + response.getStatusInfo() + " | " + (System.currentTimeMillis() - time) + " ms");
		return response;
	}
	
	public Response getCharacters(int limit, int offset) {
		String operation = "characters";
		return this.executeGet(operation, limit, offset);
	}
	
	public Response getComics(int limit, int offset) {
		String operation = "comics";
		return this.executeGet(operation, limit, offset);

	}

	public Response getEvents(int limit, int offset) {
		String operation = "events";
		return this.executeGet(operation, limit, offset);

	}
	
	//http://gateway.marvel.com/v1/public/series?ts=1478785656836&apikey=19554a34e7f49a1956e56ce9e2cb99bc&hash=da060e1e6f6e33c4e4e63c397aa9f3fa&limit=5
	public Response getSeries(int limit, int offset) {
		String operation = "series";
		return this.executeGet(operation, limit, offset);
	}

	MarvelEntityList getAll(String operation, int maxSize, int limit) {
		MarvelEntityList list = new MarvelEntityList(operation);
		// get first payload		
		String str = getPayloadFromResponse(this.executeGet(operation, 1, 0));
		JsonObject obj = JsonUtils.getObjectFromString(str);
		// get total amount
		int total = obj.getAsJsonObject("data").get("total").getAsInt();
		list.setTotal(total);
		LOG.debug("Found: " + total + " entries");
		if (maxSize != -1 && total > maxSize){
			total = maxSize;
		}
		int steps = total / limit; // number of steps required to get all data
		if (total % limit !=0){ // if there are left overs
			steps++;
		}
		if (maxSize < limit){
			limit = maxSize;
		}
		LOG.debug("Found: " + total + " entries | " + steps + " steps");
		// iterate through each offset
		for (int i = 0; i < steps; i++){
			LOG.info("Getting list of "+operation+": " + i + " / " + steps);
			str = getPayloadFromResponse(getCharacters(limit, i));
			obj = JsonUtils.getObjectFromString(str);
			JsonArray results = obj.getAsJsonObject("data").getAsJsonArray("results");
			for (int j = 0; j < results.size(); j++){
				list.addObject(results.get(i).getAsJsonObject());
				list.setSize(list.getSize()+1);
			}
		}
		LOG.info(list.getSize());
	
		return list;
		
	}
}
