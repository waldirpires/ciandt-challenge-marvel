package br.com.treinamento.dojo.service;

import java.util.Set;

import br.com.treinamento.dojo.entity.characters.MarvelCharacterResponse;
import br.com.treinamento.dojo.entity.characters.ResultElement;
import br.com.treinamento.dojo.entity.series.MarvelSeriesResponse;

/**
 * Interface using Java POJOs from JSON payloads
 * @author wrpires
 *
 */
public interface MarvelClientApi {

	MarvelCharacterResponse getCharacter(String id);

	Set<ResultElement> getAllCharacters(int maxSize, int limit);

	MarvelCharacterResponse getMarvelCharactersFromPayload(String payload);

	MarvelSeriesResponse getSpecificSeries(String id);

	MarvelCharacterResponse getCharacterResponseFromPayload(String payload);

	MarvelSeriesResponse getSeriesResponseFromPayload(String payload);

	

}