package br.com.treinamento.dojo.entity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonObject;

public class MarvelEntityList {

	private int size;
	private int total;
	private Set<Object> list;
	private String operation;
	
	public MarvelEntityList(){
		this.list = new HashSet<>();
	}
	
	public MarvelEntityList(String operation){
		this();
		this.operation = operation;
	}
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public Set<Object> getList() {
		return Collections.unmodifiableSet(list);
	}
	public void addObject(Object character) {
		this.list.add(character);
	}	
	
	public String getOperation() {
		return operation;
	}
	
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
}
