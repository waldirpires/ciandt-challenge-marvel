package br.com.treinamento.dojo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.JsonObject;

import br.com.treinamento.dojo.entity.MarvelEntityList;
import br.com.treinamento.dojo.entity.MarvelSeriesWithCharactersInfo;
import br.com.treinamento.dojo.service.MarvelSeriesClientImpl2;
import br.com.treinamento.dojo.utils.JsonUtils;

@RestController
public class MarvelController
{

    @RequestMapping (value = "/completeSeries", method = RequestMethod.GET)
    public ResponseEntity<String> getCompleteSeriesWithCharacters(@RequestParam(value="id") String id	)
    {
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	MarvelSeriesWithCharactersInfo completeSeriesWithCharacters = restClient.getCompleteSeriesWithCharacters(id);
        return new ResponseEntity<String>(JsonUtils.convertObjToJson(completeSeriesWithCharacters), HttpStatus.OK);
    }

    @RequestMapping (value = "/characters/all", method = RequestMethod.GET)
    public ResponseEntity<String> getAllCharacters(@RequestParam(value="max", defaultValue="500") String max)
    {
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	MarvelEntityList list = restClient.getAllCharacters(Integer.parseInt(max), 100);    	
        return new ResponseEntity<String>(JsonUtils.convertObjToJson(list), HttpStatus.OK);
    }
    
    @RequestMapping (value = "/characters", method = RequestMethod.GET)
    public ResponseEntity<String> getSpecificCharacter(@RequestParam(value="id") String id)
    {
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	JsonObject obj = restClient.getSpecificCharacter(id);
        return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
    }
    
    @RequestMapping (value = "/series", method = RequestMethod.GET)
    public ResponseEntity<String> getSpecificSeries(@RequestParam(value="id") String id)
    {
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	JsonObject obj = restClient.getSpecificSeries(id);
        return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
    }
    
    @RequestMapping (value = "/series/all", method = RequestMethod.GET)
    public ResponseEntity<String> getAllSeries(@RequestParam(value="max", defaultValue="500") String max)
    {
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	MarvelEntityList list = restClient.getAllSeries(Integer.parseInt(max), 100);    	
        return new ResponseEntity<String>(JsonUtils.convertObjToJson(list), HttpStatus.OK);
    }
    
}
